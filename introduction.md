---
marp: true
theme: gaia
title: 计算机系统结构
# size: 4:3
---

<!-- _class: lead -->

# 课堂简介

计算机学院 & 武汉光电国家研究中心
华中科技大学
2024-03-04 至 2024-04-24

---

## 课程资料

<style scoped>
  li {
    font-size: 20px;
  }
</style>

- 教材: [计算机系统结构教程（第2版）](http://www.tup.tsinghua.edu.cn/booksCenter/book_05056301.html), 清华大学出版社, 2014
- 参考书
  - [**计算机系统结构（微课版）**](https://www.ptpress.com.cn/shopping/buy?bookId=43961df3-4a07-4e0e-8b0f-bdf3b21d695f), 人民邮电出版社, 2024
  - [John Hennessy](https://hennessy.stanford.edu/), [David Patterson](https://www2.eecs.berkeley.edu/Faculty/Homepages/patterson.html), [**Computer Architecture: A Quantitative Approach**, 6th Edition.](https://www.elsevier.com/books/computer-architecture/hennessy/978-0-12-811905-1), 2017
  - [**计算机体系结构：量化研究方法**（第6版）](https://item.jd.com/13427803.html), 人民邮电出版社, 2017
- 课堂实验 <https://github.com/cs-course/computer-architecture-experiment>

![height:300px](images/text-book-1.png) ![height:300px](images/text-book-3.jpg) ![height:300px](images/caqa6e.jpg) ![height:300px](images/caqa6e-cn.jpg)

---

## 教师信息（前半）

- **周可** 教授，武汉光电国家研究中心，存储部，C537
- 联系方式
  - EMail zhke (AT) hust.edu.cn
  - 电话 027-87793548
  - 主页
    - <http://faculty.hust.edu.cn/zhouke2/zh_CN/index.htm>

---

## 教师信息（后半）

- **施展** 副教授，武汉光电国家研究中心，存储部，C523
- 联系方式
  - EMail zshi (AT) hust.edu.cn
  - 电话 027-87792450
  - 主页
    - <https://shizhan.github.io/>, <https://shi_zhan.gitee.io/>
    - <http://faculty.hust.edu.cn/shizhan/zh_CN/index.htm>

---

## 授课目标

- 从系统层面认识计算机
- 建立整机系统概念
- 总体设计与设计策略

---

## 评分构成

- **作业** *20%*
  - 每章一次作业
- **实验** *20%*
  - [头哥](https://www.educoder.net/classrooms/EW9YL38W?code=2HSODB)，2HSODB
- **考试** *60%*

---

## 课程计划

<style scoped>
  table {
    width: 100%;
  }
  th {
    background: #007FFF;
    font-size: 20px;
  }
  td, p, li {
    font-size: 16px;
  }
</style>

|    | 课堂内容 | 时间 | 地点（周二） |地点（周四） |
| :- |:--------|:---| :--- | :--- |
| 00 | [计算机系统结构基础](pdfs/第1章-概述.pdf) | 03-04 3,4节 | 西十二楼N307 ||
| 01 | [计算机系统结构基础](pdfs/第1章-概述.pdf) | 03-06 3,4节 || 西十二楼N402 |
| 02 | [流水线技术](pdfs/第3章-流水线技术.pdf) | 03-11 3,4节 | 西十二楼N307 ||
| 03 | [流水线技术](pdfs/第3章-流水线技术.pdf) | 03-13 3,4节 || 西十二楼N402 |
| 04 | [指令级并行](pdfs/第5章-指令级并行.pdf) | 03-18 3,4节 | 西十二楼N307 ||
| 05 | [指令级并行](pdfs/第5章-指令级并行.pdf) | 03-20 3,4节 || 西十二楼N402 |
| 06 | [存储系统](pdfs/第7章-存储系统.pdf) | 03-25 3,4节 | 西十二楼N307 ||
| 07 | [存储系统](pdfs/第7章-存储系统.pdf) | 03-27 3,4节 || 西十二楼N402|
| 08 | [存储系统](pdfs/第7章-存储系统.pdf) | 04-01 3,4节 | 西十二楼N307 ||
| 09 | [存储系统](pdfs/第7章-存储系统.pdf) | 04-03 3,4节 || 西十二楼N402 |
| 10 | [I/O系统](pdfs/第8章-IO系统.pdf) [2024更新](pdfs/Ch6-外存系统.pdf) | 04-08 3,4节 | 西十二楼N307 ||
| 11 | [I/O系统](pdfs/第8章-IO系统.pdf) | 04-10 3,4节 || 西十二楼N402 |
| 12 | [互联网络](pdfs/第9章-互联网络.pdf) [2024更新](pdfs/Ch8-多处理器.pdf) | 04-15 3,4节 | 西十二楼N307 ||
| 13 | [互联网络](pdfs/第9章-互联网络.pdf) | 04-17 3,4节 || 西十二楼N402 |
| 14 | [多处理机](pdfs/第10章-多处理机.pdf) | 04-22 3,4节 | 西十二楼N307 ||
| 15 | 习题参考 | 04-24 3,4节 || 西十二楼N402 |
||**考试**| **TBA** | TBA ||

<!-- [习题参考](pdfs/习题参考答案.pdf) -->
