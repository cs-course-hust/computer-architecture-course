---
marp: true
theme: gaia
title: 计算机系统结构
# size: 4:3
---

<!-- _class: lead -->

# 课堂简介

计算机学院 & 武汉光电国家研究中心
华中科技大学
2024-03-12 至 2024-05-02

---

## 课程资料

<style scoped>
  li {
    font-size: 20px;
  }
</style>

![bg right 60%](images/text-book-1.png)

- 教材
  - [计算机系统结构教程（第2版）](http://www.tup.tsinghua.edu.cn/booksCenter/book_05056301.html)， 清华大学出版社，2014
- 参考书
  - [John Hennessy](https://hennessy.stanford.edu/), [David Patterson](https://www2.eecs.berkeley.edu/Faculty/Homepages/patterson.html), [**Computer Architecture: A Quantitative Approach**, 6th Edition.](https://www.elsevier.com/books/computer-architecture/hennessy/978-0-12-811905-1), 2017
  - [**计算机体系结构：量化研究方法**（第6版）](https://item.jd.com/13427803.html), 人民邮电出版社, 2017
- 课程资料
  - 本页面 <https://cs-course-hust.gitlab.io/computer-architecture-course/introduction>
  - 课堂实验 <https://github.com/cs-course/computer-architecture-experiment>

---

## 教师信息（前半）

- **周可** 教授，武汉光电国家研究中心，存储部，C537
- 联系方式
  - EMail zhke (AT) hust.edu.cn
  - 电话 027-87793548
  - 主页
    - <http://faculty.hust.edu.cn/zhouke2/zh_CN/index.htm>

---

## 教师信息（后半）

- **施展** 副教授，武汉光电国家研究中心，存储部，C523
- 联系方式
  - EMail zshi (AT) hust.edu.cn
  - 电话 027-87792450
  - 主页
    - <https://shizhan.github.io/>, <https://shi_zhan.gitee.io/>
    - <http://faculty.hust.edu.cn/shizhan/zh_CN/index.htm>

---

## 授课目标

- 从系统层面认识计算机
- 建立整机系统概念
- 总体设计与设计策略

---

## 评分构成

- **作业** *20%*
  - 每章一次作业
- **实验** *20%*
  - [头哥](https://www.educoder.net/classrooms/EW9YL38W?code=2HSODB)，2HSODB
- **考试** *60%*

---

## 课程计划

<style scoped>
  table {
    width: 100%;
  }
  th {
    background: #007FFF;
    font-size: 20px;
  }
  td, p, li {
    font-size: 16px;
  }
</style>

|    | 课堂内容 | 时间 | 地点（周二） |地点（周四） |
| :- |:--------|:---| :--- | :--- |
| 00 | [计算机系统结构基础](pdfs/第1章-概述.pdf) | 03-12 3,4节 | 西十二楼N112 ||
| 01 | [计算机系统结构基础](pdfs/第1章-概述.pdf) | 03-14 3,4节 || 西十二楼S110 |
| 02 | [流水线技术](pdfs/第3章-流水线技术.pdf) | 03-19 3,4节 | 西十二楼N112 ||
| 03 | [流水线技术](pdfs/第3章-流水线技术.pdf) | 03-21 3,4节 || 西十二楼S110 |
| 04 | [指令级并行](pdfs/第5章-指令级并行.pdf) | 03-26 3,4节 | 西十二楼N112 ||
| 05 | [指令级并行](pdfs/第5章-指令级并行.pdf) | 03-28 3,4节 || 西十二楼S110 |
| 06 | [存储系统](pdfs/第7章-存储系统.pdf) | 04-02 3,4节 | 西十二楼N112 ||
| 07 | [存储系统](pdfs/第7章-存储系统.pdf) | ~~04-04 3,4节~~ *(清明)* || 西十二楼S110 |
| 08 | [存储系统](pdfs/第7章-存储系统.pdf) | 04-09 3,4节 | 西十二楼N112 ||
| 09 | [存储系统](pdfs/第7章-存储系统.pdf) | 04-11 3,4节 || 西十二楼S110 |
| 10 | [I/O系统](pdfs/第8章-IO系统.pdf) [2024更新](pdfs/Ch6-外存系统.pdf) | 04-16 3,4节 | 西十二楼N112 ||
| 11 | [I/O系统](pdfs/第8章-IO系统.pdf) | 04-18 3,4节 || 西十二楼S110 |
| 12 | [互联网络](pdfs/第9章-互联网络.pdf) [2024更新](pdfs/Ch8-多处理器.pdf) | 04-23 3,4节 | 西十二楼N112 ||
| 13 | [互联网络](pdfs/第9章-互联网络.pdf) | 04-25 3,4节 || 西十二楼S110 |
| 14 | [多处理机](pdfs/第10章-多处理机.pdf) | 04-28 3,4节 *(劳动节05-02**周四**调休)* || **西十二楼S110** |
| 15 | [习题参考](pdfs/习题参考答案.pdf) | 04-30 3,4节 | 西十二楼N112 ||
||**考试**| **05-26 下午15:00-17:30** | 西十二楼N103/104 ||

<!-- [习题参考](pdfs/习题参考答案.pdf) -->
